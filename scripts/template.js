const closePopUp = document.getElementById('close-popup');
const popup = document.getElementById('popup');
const templates = document.getElementById('templates');
closePopUp.addEventListener('click', () => {
        fetchData();
        popup.style.display='none';
        templates.style.display='block';
    }
);
document.addEventListener('click',(e)=>{
    if(e.target.className === 'edit'){
        fetchData();
        popup.style.display='inline-block';
        templates.style.display = 'none';
    }
});
 function fetchData() {
    const title = document.getElementById('header');
    const content = document.getElementById('content');
     fetch('./text/title')
        .then(response => response.text())
        .then(data => {
            title.innerHTML = data;
        });
     fetch('./text/content')
        .then(response => response.text())
        .then(data => {
            content.innerHTML = data;
        });
}